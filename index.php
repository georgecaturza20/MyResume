<!doctype html>
<head>
   <meta charset='utf-8'>
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <meta name="viewport" content="width=device-width, initial-scale=1">
   <link rel="stylesheet" href="css/styles.css">
   <script src="http://code.jquery.com/jquery-latest.min.js" type="text/javascript"></script>
   <script src="script.js"></script>

   <link rel="icon" type="image/jpg" href="images/icon.jpg">
   
   <title>George's Resume</title>
</head>
<body>
    <style>
        @font-face {
            font-family: Another day in Paradise;
            src: url('MyResume/fonts/Another day in Paradise.ttf')}
        @font-face {
            font-family: 'Gobold Uplow Italic';
            src: url('MyResume/fonts/Gobold Uplow Italic.otf')}

           
        body {background-color:#00bd9c;}
        h1 {color: #00bd9c; font-family: 'Another day in Paradise';}
        p {color: #33495f; font-family: 'Gobold Uplow'; font-size: 30px;}
        div.a { 
            font-size: 80px;
            position: absolute;
            left: 380px;
            top: -20px;
        } 
        div.b {   
            font-size: 80px;
            position: absolute;
            left: 410px;
            top: 280px;
        }
        div.image1 img { 
            z-index: -1;
            display: block;
            margin-left: auto;
            margin-right: auto;
            border-radius: 50%;
            width: 60%;
            height: auto;
        }
    </style>

<div id='cssmenu'>
<ul>
   <li class='active'><a href='index.html'>Welcome</a></li>
   <li><a href='resume.html'>To My Resume</a></li>
</ul>
</div>

<div class = "a"> <h1>HELLO!</h1> </div>
<div class="b">
    <br>
    <br>
    <p>"My First Repository Project, Life is Here!"</p>
</div>
<br>
<div class = "image1"><img src="images/gif1.gif" alt="explore"></div>
</body>
<html>
