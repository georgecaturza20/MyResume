<!doctype html>
<head>
   
    <meta charset='utf-8'>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="css/styles.css">
    <script src="http://code.jquery.com/jquery-latest.min.js" type="text/javascript"></script>
    <script src="script.js"></script>

    <link rel="icon" type="image/jpg" href="images/icon.jpg">
    <title>George's Resume</title>
    
<style> 
    div{
        background-color:#00bd9c;
        width: 1000px;
        border: auto solid;
        margin: auto;
    }
    
    .center{
        text-align: center;
    }   
</style>
<div id='cssmenu'>
    <ul>
    <li><a href='index.html'>Welcome</a></li>
    <li class='active'><a href='resume.html'>To My Resume</a></li>
    </ul>
    </div>
    
</head>  
<body background= "images/gif2.gif"> 
<div>
<div class="center">
  <br><br>      
<h1>RESUME</h1>
</div>
<div class="center">
<img src="images/profile.jpg" alt="Avatar" style="width: 200px">
</div>
<div class="center">
<h4>--------------Instruction: Please fill all the blanks in CAPITAL LETTERS. Thank you!--------------</h4>
</div>
<div class="center">
    <h2>PERSONAL INFORMATION</h2>
<br>
Last name:
<input type="text" name="lastname" value="CATURZA">
First Name:
<input type="text" name="firstname" value="GEORGE JR.">
Middle Name:
<input type="text" name="middlename" value="CAGABCAB">
<br><br>
Gender: 
<input type="text" name="gend" value="MALE">
Age:  
<input type="number" name="age" value="20">
Date of Birth:
<input type="text" name="month" value="August 21, 1998">
<br><br>
Present Address:
<input type="text" name="address" value="BLK. 3, LT. 18, NHA MAA">
City/Province:
<input type="text" name="city" value="DAVAO CITY">
Zip Code:
<input type="number" name="zip" value="8000">
<br><br>
Contact #:
<input  type="number" name="number" value="09485358210">
Telephone #:
<input type="tel" name="number" value="(082)282-2060">
<br><br>
Tin #:
<input type="number" name="tin" value="">
Position Desires: 
<input type="text" name="position" value="WEB DEVELOPER">
<br><br>
<h2>FAMILY'S INFORMATION</h2>
Father's name: 
<input type="text" name="fullname" value="GEORGE G. CATURZA SR.">
Occupation:
<input type="text" name="occu" value="PROFESSIONAL DRIVER">
<br><br>
Mother's maiden name:
<input type="text" name="fullmaid" value="MARISA O. CAGABCAB">
Occupation:
<input type="text" name="occu" value="PUBLIC TEACHER">
<br><br>
Sibling(s) Information (if applicable):
<input type="text" name="name" value="GEMLYN C. CATURZA">
<h2>OBJECTIVE</h2>
<textarea rows="2" cols="100">
    I WANT TO ENHANCE AND PRACTICE MY SKILLS IN WEB DEVELOPING IN ORDER TO HELP THE COMPANY TO REACH THEIR GOALS.
</textarea>
<h2>SKILLS</h2>
<textarea rows="8" cols="70">
* KNOWLEDGEABLE ABOUT THE BASICS OF PROGRAMMING. 
* CAN WORK OVERTIME IF NEEDED.
* CAN WORK MULTIPLE TASKS.
* HAVE THE CONFIDENCE TO TALK TO OTHER PEOPLE.     
* WILLING TO BE TRAINED. 
* CAN THINK WITH NEW IDEAS.
* KNOWLEDGEABLE IN ASSEMBLING AND DISASSEMBLING COMPUTER HARDWARES.
* ENABLE TO LEARN THINGS IMMEDIATELY IF IT IS HANDS-ON.
    </textarea>
<h2>EDUCATION</h2>
Elementary Education:
<textarea rows="1" cols="50">
LEON A. GARCIA SR. ELEMENTARY SCHOOL
</textarea>
<br><br>
Secondary Education:
<textarea rows="1" cols="50">
GREEN MEADOW CHRISTIAN ACADEMY OF DAVAO
</textarea>
<br><br>
Tertiary Education:
<textarea rows="1" cols="40">
UNIVERSITY OF SOUTHEASTERN PHILIPPINES
</textarea>
<br><br>
Course:
<textarea rows="2" cols="48">
BACHELOR OF SCIENCE IN INFORMATION TECHNOLOGY    
</textarea>
<h2>ACTIVITIES</h2>
<textarea rows="1" cols="90">
PROPERTY SPECIALIST IN SUNTRUST PROPERTIES INCORPORATION A SUBSIDIARY OF MEGAWORLD CORPORATION.
</textarea>
<br><br>
</div>
</div>
</body>
<html>
